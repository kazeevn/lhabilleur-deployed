from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^la_habilleur/', include('la_habilleur.urls',
                                   namespace='la_habilleur')),
    url(r'^admin/', include(admin.site.urls)),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
)
