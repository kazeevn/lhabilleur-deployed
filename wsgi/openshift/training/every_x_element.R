input <- read.table("Rating.dat", header = FALSE, sep = " ");
# Delete first useless column
input  = input[, c(F, 1:19>0)]
# number of features
nfeach <- ncol(input)-1
# number of objects
nobj = nrow(input)

# every n-th object
n = 4
train = input[c((1:nobj)%%n==0),]
test_index1 = c((1:nobj)%%n!=0)
test_index2 = c(1:18>0, F)
test = input[test_index1, test_index2]
class = input[test_index1, 19]

# training
num_of_trees = 100
res <- randomForest ( x=as.matrix(train[,1:nfeach]), y=factor(train[,nfeach+1]), ntree=num_of_trees)
# classification
answer <- predict(res, as.matrix(test), type="response", norm.votes=TRUE, predict.all=FALSE, proximity=FALSE, nodes=FALSE)
answer <- as.vector(answer)

output <- file('output.txt', "w")
cat(answer, file = output, sep="\n")
close(output)

# error on control
sum(c(answer != class))

# total deviation
sum(abs(as.numeric(answer)-class))
