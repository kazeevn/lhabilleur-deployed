# -*- coding: utf-8 -*-
import unittest
from pair_ranker import RankerPairSimple
from cloth_db import ClothDB, Cloth, assemble_suit

# ClothDB is considered to be present always.
cloth_db_ = ClothDB()
cloth_db_.load_from_file('data/ItemsList.dat')


class TestRank(unittest.TestCase):
    def setUp(self):
        self.ranker = RankerPairSimple()
        self.ranker.load_from_names_file('data/pair_ranks.txt',
                                 lambda name: cloth_db_.get_by_name(name).id)
        self.rank = self.ranker.rank

    def testNikitaSummer(self):
        import numpy as np
        trousers = np.array(((70, 11, 93), (55, 14, 69), (67, 24, 53)))
        jacket = np.array(((66, 14, 57), (76, 10, 44), (70, 15, 76)))
        shirt = np.array(((211, 76, 82), (191, 15, 100)))

        nikita_summer_raw = [trousers, jacket, shirt]
        nikita_summer_clothes = map(lambda name:
                                    cloth_db_.get_by_name(name).id,
                                    [u"Брюки", u"Пиджак", u"Рубашка"])
        nikita_summer_colors = map(
            lambda piece: map(round, np.mean(piece, axis=0)),
            nikita_summer_raw)
        self.assertEqual(
            self.rank(assemble_suit(
                nikita_summer_clothes, nikita_summer_colors)), 2)

    def testGirls(self):
        test1_clothes = map(lambda name: cloth_db_.get_by_name(name).id,
                            [u"Футболка", u"Джинсы", u"Кроссовки"])
        test1_colors = [[14, 42, 86], [244, 60, 54], [244, 0, 100]]

        test2_clothes = map(lambda name: cloth_db_.get_by_name(name).id,
                            [u"Кофта", u"Юбка", u"Туфли"])
        test2_colors = [[235, 87, 90], [215, 87, 82], [244, 90, 33]]

        test3_clothes = map(lambda name: cloth_db_.get_by_name(name).id,
                            [u"Кофта", u"Джинсы", u"Туфли"])
        test3_colors = [[40, 100, 90], [4, 86, 78], [360, 90, 85]]
        black_white_clothes = map(lambda name: cloth_db_.get_by_name(name).id,
                                  [u"Блузка", u"Юбка"])
        black_white_colors = [[0, 0, 100], [0, 0, 0]]

        test_set = map(lambda item: assemble_suit(*item),
            ([test1_clothes, test1_colors],
             [test2_clothes, test2_colors],
             [test3_clothes, test3_colors],
             [black_white_clothes, black_white_colors])
            )
        self.assertEqual(map(self.rank, test_set), [2, 3, 2, 3])

if __name__ == '__main__':
    unittest.main()
