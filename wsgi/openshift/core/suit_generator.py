from pair_ranker import *

class SuitGenerator:
    """Generator class for generating all possible suits from a
    wardrobe. Returns id lists."""

    def iterate_var_nitems(self, slots_aviable):
        for n in range(2, len(slots_aviable)+1):
            generator = self.iterate_over(slots_aviable, n)
            try:
                while True:
                    yield generator.next()
            except StopIteration:
                pass

    def iterate_over(self, slots_aviable, num_items):
        if num_items > len(slots_aviable):
            raise IndexError("More items requiered than left.")
        if num_items == 1:
            for slot in slots_aviable:
                for item in self.clothes[slot]:
                    yield [item.id,]
        else:
            # First, we try everything with us included
            for item in self.clothes[slots_aviable[0]]:
                gen_next = self.iterate_over(slots_aviable[1:], num_items - 1)
                for next_item in gen_next:
                    yield [item.id,] + next_item

            # Then we pass the baton
            # num_items may be
            # 1. more than available slots = bug
            # 2. equal = it's no use passing - no suit can be formed without us
            # 3. less = OK, thay can do without us, pass
            if num_items < len(slots_aviable):
                gen_next = self.iterate_over(slots_aviable[1:], num_items)
                for next_item in gen_next:
                    yield next_item

    def __init__(self, wardrobe, num_items = None):
        self.clothes = wardrobe.clothes_by_slot
        slots = wardrobe.slots()
        if num_items is None:
            generator = self.iterate_var_nitems(slots)
        else:
            generator = self.iterate_over(slots, num_items)
        self.next = generator.next
        self.__iter__ = generator.__iter__
