# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import unittest
from pair_ranker import RankerPairSimple
from cloth_db import ClothDB, Cloth, assemble_grey_suit, assemble_suit

# ClothDB is considered to be present always.
cloth_db_ = ClothDB()
cloth_db_.load_from_file('data/ItemsList.dat')


def names2ids(names):
    return map(lambda name: cloth_db_.get_by_name(name).id, names)


def get_id(item):
    return item.id


class TestFillSlot(unittest.TestCase):
    def setUp(self):
        self.ranker = RankerPairSimple()
        self.ranker.load_from_names_file('data/pair_ranks.txt',
                                 lambda name: cloth_db_.get_by_name(name).id)
        self.fill = self.ranker.fill_slot

    def testFormal(self):
        suit = names2ids((u'Блузка', u'Туфли', u'Пиджак'))
        items = [13, 11, 10, 12]
        recommendation = self.fill(*map(assemble_grey_suit,
                                        [suit, items]), num=4)
#        for item, rank in recommendation:
#            name = cloth_db_.get_by_id(item.id).name
#            print item.id, name, rank
        self.assertEqual(map(lambda item: item[0].id, recommendation),
                         [11, 12, 10, 13])

    def testFormalSkrewedColor(self):
        suit = names2ids((u'Блузка', u'Туфли', u'Пиджак'))
        colors = [(242, 100, 29), ] * 3  # Nice blue suit
        # https://image.guim.co.uk/sys-images/Guardian/Pix/pictures/2012/7/4/1341387892952/Margaret-Thatcher-prime-m-001.jpg
        items = [Cloth(10, (359, 100, 100)),  # Very red trousers
                 Cloth(13, colors[0]),  # Matching shorts
                 Cloth(11, colors[0])]  # Matching jeans

        recommendation = self.fill(assemble_suit(suit, colors), items, num=3)
#        for item, rank in recommendation:
#            name = cloth_db_.get_by_id(item.id).name
#            print name, rank

        self.assertEqual(map(lambda item: item[0].id, recommendation),
                         [11, 10, 13])

if __name__ == '__main__':
    unittest.main()
