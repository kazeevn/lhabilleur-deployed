import sys

# The script accepts following filenames
# input: Clothes list in form (slot, id, name)
# output pais list in form (slot1, id1, slot2, id2, rating)
# slot1 = slot2 => 0, not in file

def ask(item1, item2):
    rating = -1
    while rating < 0 or rating > 3:
        rating = int(raw_input("Rate 0-3 %s + %s "%(item1[2], item2[2])))
    return rating

infile = open(sys.argv[1], 'r')
outfile = open(sys.argv[2], 'w')

clothes = list()
for line in infile:
    clothes.append(line.split(' '))
infile.close()
N = len(clothes)
for i in range(N):
    item1 = clothes[i]
    for j in range(i+1, N):
        item2 = clothes[j]
        if not item2[0] == item1[0]:
            outfile.write("%s %s %s %s %d\n"%(item1[0], item1[1],
                                              item2[0], item2[1],
                                              ask(clothes[i], clothes[j])))
outfile.close()
