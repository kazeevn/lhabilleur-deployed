from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.shortcuts import get_object_or_404, render
import core
from la_habilleur.models import Wardrobe
from django.utils.datastructures import MultiValueDictKeyError

def index(request):
    return HttpResponse("Hello")

def display_wardrobe(request):
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    return render(request, 'la_habilleur/add_cloth.html', {
        'the_wardrobe': the_wardrobe.wardrobe,
        'cloth_db':  core.cloth_db.ClothDB().clothes_by_slot})

def add_to_wardrobe(request):
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    try:
        id_to_add = core.cloth_db.ClothDB().get_by_name(
            request.POST['cloth_name']).id
    except (ValueError, MultiValueDictKeyError, KeyError):
        return render(request, 'la_habilleur/add_cloth.html', {
         'the_wardrobe': the_wardrobe.wardrobe,
            'cloth_db': core.cloth_db.ClothDB().clothes_by_slot,
            "error_message": "Invalid item"})

    cloth_to_add = core.cloth_db.Cloth(id_to_add, (0, 0, 0))
    if cloth_to_add in the_wardrobe.wardrobe:
        return render(request, 'la_habilleur/add_cloth.html', {
            'the_wardrobe': the_wardrobe.wardrobe,
            'cloth_db': core.cloth_db.ClothDB().clothes_by_slot,
            "error_message": "Duplicate entry"})
    the_wardrobe.wardrobe.append(cloth_to_add)
    the_wardrobe.save()
    return HttpResponseRedirect(reverse('la_habilleur:display-wardrobe'))

def drop_wardrobe(request):
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    the_wardrobe.wardrobe = []
    the_wardrobe.save()
    return HttpResponseRedirect(reverse('la_habilleur:display-wardrobe'))

def enter_suit(request):
    return render(request, 'la_habilleur/calc_suit_rank.html', {})

def view_suit_rank(request):
    cloth_ids = request.POST['suit']
    cloth_ids = cloth_ids.split()
    cloth_ids = map(int, cloth_ids)
    ranker = core.pair_ranker.RankerPairSimple()
    rank = ranker.rank_items(cloth_ids)
    return HttpResponse(rank)
